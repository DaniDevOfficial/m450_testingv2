﻿### Testkonzept 
**

#### Mitbeteiligte:
- David Bischof
- Aakash Sethi

Erstellt am 21.12.2024

**


#### 1. Backend-Tests: REST - Backend Json Server auf Port 3001 (Postman)

-   **Ziel**: Das Ziel ist es, mit Postman REST API-Endpunkte eines JSON-Servers auf dem Port 3001 zu testen.
-   **Schritte**:
    -   **JSON Server einrichten**: JSON-Server auf dem Port 3001 einrichten (falls nicht schon eingerichtet). 
    -   **Postman Collections erstellen**: Anfragen für verschiedene API-Endpunkte in Collections organisieren.
    -   **Testfälle schreiben**: Für jeden Endpunkt Testfälle erstellen, um Antworten auf verschiedene Szenarien zu validieren (z.B. GET, POST, PUT, DELETE-Anfragen).
    -   **Tests durchführen**: Tests manuell oder über Postmans Collection Runner ausführen. Überprüfung von Statuscodes, Antwortkörper, Headern und Antwortzeiten (Response Types).
    -   **Automatisierung**: Optional kann Postmans Newman-Tool für die Ausführung über die Kommandozeile und Integration in CI/CD-Pipelines verwendet werden.

#### 2. Schnittstellentests: Postman Json Server Port 3001 für die Trip Entity

-   **Ziel**: Speziell die Schnittstellen testen, die mit der Trip Entität in Ihrer Anwendung zusammenhängen.
-   **Schritte**:
    -   **Trip-Endpunkte identifizieren**: Alle API-Endpunkte definieren, die mit der Trip-Entität interagieren (z.B. Erstellung einer Reise, Abrufen von Reisedetails).
    -   **Testfälle erstellen**: Testfälle für CRUD-Operationen, Randfälle und Fehlerbehandlung speziell für die Trip-Entität entwickeln.
    -   **Datenvalidierung**: Sicherstellen, dass die Datenstruktur und Datentypen der Antwort wie erwartet sind.
    -   **Parameter Tests**: Mit verschiedenen Parametern testen, um die Robustheit zu gewährleisten (z.B. verschiedene Trip-IDs, Benutzerrollen).

#### 3. Komponententests mit Jest

-   **Ziel**: Testen von React-Komponenten mit Jest.
-   **Einrichtung**:
    -   **Jest konfigurieren**: Jest im Projekt einrichten.
    -   **Snapshot-Tests**: Als Referenz Snapshots erstellen
    -   **Unit-Tests**: Unit-Tests für einzelne Komponenten schreiben, mit Fokus auf Funktionalität.
    -   **Mocking**: Mocks verwenden, um Komponenten isoliert zu testen (besonders solche, die mit APIs oder externen Diensten interagieren).

#### 4. Benutzerinteraktionstests

-   **Ziel**: Testen der Benutzerinteraktionsaspekte Ihrer Anwendung.
-   ## **Test-Szenarien**:
    

**Navigation**: Testen des Benutzerflusses durch die Anwendung (User Experience).

-   **Responsive Design**: Sicherstellen, dass die Anwendung sich korrekt auf verschiedenen Geräten und Bildschirmgrößen verhält.
-   **Fehlerbehandlung**: Testen, wie die Anwendung Benutzerfehler behandelt (z.B. falsche Eingaben).


**

## Tools
- **Postman** 
- **JEST**
- **React-testing-library**


 ## Was getestet wird
 - **App.jsx**
 - **Wishlist.jsx**



Dieses Testkonzept entspricht den Testempfehlungen, sowie selbstständig erarbeitete Tests.  

**







