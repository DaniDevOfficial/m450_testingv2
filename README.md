## Testing Plan Outline:

### 1. Introduction

- *Project Overview:*
  - BizTip is a App where Users can wishlist their favorite Buissnes trips, with Start and end dates as well as Cost calculation.
  - The App is Built on a JSON server as a backend and the magnificant frontend is created with native React.

- *Testing Goals:*
  - The Goal with the testing of this application is to find weak spots where the User could have a bad expirience. The tests also do check for some rendering issuses, so the frontend is acctualy displayed correctly
  - The test are mainly going over all the small features of the application like Adding a new Item to the wishlist, fetching form the json server or deleting a item form the Wishlist. 

### 2. Testing Types

- *White Box Testing:*
  - Most of the application is testet in White Box tests in the tests folder
  - For the White Box testing we used Jtest for React/Javascript

- *Postman Tests:*
  - To test the Json server API we used in addition to the Jtests also a Postman test



### 3. Test Environment

- *Testing Environment:*
  - The tests can be executed on a local maschine with `npm test` or also in a GitLab CI/CD Pipeline.
  - For the Pipeline the tests are used to check if the application is faulty. When this is the case the Testing stage fails and the not working app isnt deployed

- *Dependencies and Mocking:*
  - The testing of the App itself just simply need the node modules with `npm install` installed. For the Json server tests you have to run the application.

### 4. Test Data

- *Test Data Strategy:*
  - Each test executes on its own and doesn't interfear with any other test, so the test are all completly standalone. Some test might build up apon eachother so if the first in line fails most likely the others will also fail. 
  - Its important to note that if the Json file is changed in any way the API Test will fail because we test if the data from the API is the same as the local one. 

### 5. Test Scenarios

- *Feature-wise Scenarios:*
  - API test:
    - The API has just the test `FetchAPIData()` which tests if the fetch works how intended and also checks if the fetched data is the right one. (this test has to be ajusted when the data from the API changes)
  - Triplist testing: 
    - The first test is `filters trips based on selected month where a trip is avaiable`. This test sadly doesn't work in the current build correctly because there are some errors in the frontend filter code. If this gets fixed you need to ajust the expected to be 1 and not 3
    - The second test `filters trips based on selected month where no trip is avaiable`  does a same thing as the test before expect here a month that has no Trips in it gets selected. This test works as intended.
  - Wishlist in Combination with the Triplist
    - The first test `is wishlist empty` is simply checked if the Wishlist is acctualy empty or if there is some mock data in it.
    - The second test`add to wishlist button is clicked` is simply checking if the button can even be clicked. 
    - The third test `add first trip to wishlist`  looks at the functionality of the button and checks if the trip gets added to the Wishlist.
    - With the fourth test `delete Item from wishlist` is tested if a item that has been put into the wishlist can be removed out of it again. Sadly this test also fails in the current state of the application, because the delete single item button does not work as intended
    - The fifth and last major test `delete all Items from wishlist` tests the clear button of the wishlist. This button in comparison to the single delete button acctualy works surprisingly as intended.
- *Edge Cases:*
  - Identify and document edge cases to be tested.

- *Usability Testing (rendering):*
  - The most Important Render test `render App` is the first one executed
  - The second one is the `render Header` Component that is being tested.
  - Following is the third test `render Wishlist` component tested.
  - The second to last render test is the `render Triplist` component
  - The last render test is for the Footer with the `render Foorter` test

- Postman tests
  - There is only one because the API is not that sofisticated. The test simply gets all the data from the `http://localhost:3001/trips` route. 


### 6. Test Execution

- *Test Execution Steps:*
  - To test the Application you first need to clone this repository to your local maschine
  - Now you need to `npm install` all the dependencies
  - Optional you also can start the app to test the API but its not required
  - Finlay you can run `npm test` in the console and see all the test in the test folder `tests` execute. 

- *Automation Scripts:*
  - This App is connected to a CI/CD Pipeline, which automates the testing process each time a commit is made. This is important, so the deployment only executes when the tests aren't failing. 
  - You can test this on your own for this you just need to follow these steps:
    1. Fork this Repository to your onw account
    2. Create or log into your netlify account and create a new Site
    3. Copy the site id and Auth token into GitLab Enviroment Vaiables called `NETLIFY_AUTH_TOKEN` and `NETLIFY_SITE_ID`
    4. Make a commit and push it. 
    5. Now the pipeline should execute and if you didn't change anything on the App it should automaticaly test, build and deploy to netlify. 


### 7. Test Reporting

- *Test Results:*
  - For the Local version you simply can chech in the console which tests have failed
  - In the Pipeline you have to go on the specific Job that you want to see and look there in the console what went wrong

### 8. Continuous Integration/Continuous Deployment (CI/CD)

- *Integration with CI/CD:*
  - As Stated before the Pipeline has 3 different Stages
   - Testing
    - Here the App executes all the tests and even if just one fails the whole pipeline fails.
   - Build
    - After the testing the app gets build for the next stage
   - Deployment
    - Here does the automatic deployment to netlify happen, but only when before everything went without any errors

### 9. Test Maintenance

- *Test Maintenance Plan:*
  - As soon as our frontend team fixes their two issues with the fiter and delete button will these tests be updated to work correctly.
  - Also as more features get added they will be tested indepth. 

### 10. Conclusion

- *Summary:*
  Our comprehensive testing plan for the BizTip application has been structured to ensure a robust and userfriendly experience. This has been enabled by various tests such as White Box and Postman Tests to identify weak spots. 
  The testing Enviroment is versatile, accommodating both local machines and CI/CD pipelines (GitLab). 
  Testing this application was a thoughtout process as we have focussed on testing individual features and scenarios that users could run into, such as API functionality and interactions. While we have encountered some challenges with numerous frontend elements, those are being adressed by the frontend team. We plan to update tests as new features are added / existing ones are changed or refined. 
  Automation through our CI/CD pipelines ensures that any code changes are immediately and thoroughly tested, maintaining our standard.

