import React from "react";
import Sound from "./funny/Sound.mp3";
export default function Header() {
  const [counter, setCounter] = React.useState(0);
  const [audio] = React.useState(new Audio(Sound));
  function handlyeCapy() {
    setCounter(counter + 1);
    if (counter === 20) {
      audio.play(); // is kinda loud. Pay attention
      setCounter(0);
    }
  }

  
  return (
    <header className="header">
      <nav>
        <ul>
          <li>
            <img
              onClick={handlyeCapy}
              width="150px"
              alt="biztripsLogo"
              src="/images/logo.png"
            />
          </li>
        </ul>
      </nav>
    </header>
  );
}
