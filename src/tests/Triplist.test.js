import App from "../App";
import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import TripList from "../components/TripList";
import Header from "../Header";
const testTrips = [
    {
      "id": 1,
      "title": "BT01",
      "description": "San Francisco World Trade Center on new Server/IOT/Client ",
      "startTrip": [
        2021,
        2,
        13,
        0,
        0
      ],
      "endTrip": [
        2021,
        2,
        15,
        16,
        56
      ],
      "meetings": [
        {
          "id": 1,
          "title": "One Conference",
          "description": "Key Note on One Conference"
        },
        {
          "id": 2,
          "title": "Zero Conference",
          "description": "Workshop Zero on One Conference"
        }
      ]
    },
    {
      "id": 2,
      "title": "BT02",
      "description": "Santa Clara Halley on new Server/IOT/Client",
      "startTrip": [
        2021,
        6,
        23,
        9,
        0
      ],
      "endTrip": [
        2021,
        6,
        27,
        16,
        56
      ],
      "meetings": [
        {
          "id": 3,
          "title": "One Conference",
          "description": "HandsOn on One Conference"
        },
        {
          "id": 4,
          "title": "One Conference",
          "description": "Key Note on One Conference"
        }
      ]
    },
    {
      "id": 3,
      "title": "BT03",
      "description": "San Cose City Halley on Docker/IOT/Client",
      "startTrip": [
        2021,
        12,
        13,
        9,
        0
      ],
      "endTrip": [
        2021,
        12,
        15,
        16,
        56
      ],
      "meetings": [
        {
          "id": 5,
          "title": "One Conference",
          "description": "Key Note on One Conference"
        }
      ]
    },
    {
      "id": 3,
      "title": "BT04",
      "description": "San Mateo City Halley on Docker/IOT/Client",
      "startTrip": [
        2023,
        12,
        13,
        9,
        0
      ],
      "endTrip": [
        2023,
        12,
        15,
        16,
        56
      ],
      "meetings": [
        {
          "id": 6,
          "title": "One Conference",
          "description": "Key Note on One Conference"
        }
      ]
    }
  ]
  
  const mockAddToWishlist = jest.fn();
  
/**This test doesnt work how intended due to bad frontend work (if the frontend was correct this test would work) */
test("filters trips based on selected month where a trip is avaiable (test doesnt work due to bad frontend)", () => {
    render(<TripList addToWishlist={mockAddToWishlist} />);
    const monthSelect = screen.getByLabelText("Filter by Month:");
    fireEvent.change(monthSelect, { target: { value: "2" } });
    // this test sadly fails due to the fact that the code for the filter isnt very good
    // the filter is not working as intended and just returns all trips and not only the ones in february (the test should work but the code doesnt)
    const trips = screen.getAllByText("Add to Wishlist");
    expect(trips.length).toBe(3);
  });
  
  
  test("filters trips based on selected month where no trip is avaiable", () => {
    render(<TripList addToWishlist={mockAddToWishlist} />);
    const monthSelect = screen.getByLabelText("Filter by Month:");
    fireEvent.change(monthSelect, { target: { value: "1" } });
    const trips = screen.getAllByText("Productlist is empty");
    expect(trips.length).toBe(1);
  }
  );
  