import App from "../App";
import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import TripList from "../components/TripList";
import Header from "../Header";

const testTrips = [
    {
      "id": 1,
      "title": "BT01",
      "description": "San Francisco World Trade Center on new Server/IOT/Client ",
      "startTrip": [
        2021,
        2,
        13,
        0,
        0
      ],
      "endTrip": [
        2021,
        2,
        15,
        16,
        56
      ],
      "meetings": [
        {
          "id": 1,
          "title": "One Conference",
          "description": "Key Note on One Conference"
        },
        {
          "id": 2,
          "title": "Zero Conference",
          "description": "Workshop Zero on One Conference"
        }
      ]
    },
    {
      "id": 2,
      "title": "BT02",
      "description": "Santa Clara Halley on new Server/IOT/Client",
      "startTrip": [
        2021,
        6,
        23,
        9,
        0
      ],
      "endTrip": [
        2021,
        6,
        27,
        16,
        56
      ],
      "meetings": [
        {
          "id": 3,
          "title": "One Conference",
          "description": "HandsOn on One Conference"
        },
        {
          "id": 4,
          "title": "One Conference",
          "description": "Key Note on One Conference"
        }
      ]
    },
    {
      "id": 3,
      "title": "BT03",
      "description": "San Cose City Halley on Docker/IOT/Client",
      "startTrip": [
        2021,
        12,
        13,
        9,
        0
      ],
      "endTrip": [
        2021,
        12,
        15,
        16,
        56
      ],
      "meetings": [
        {
          "id": 5,
          "title": "One Conference",
          "description": "Key Note on One Conference"
        }
      ]
    },
    {
      "id": 3,
      "title": "BT04",
      "description": "San Mateo City Halley on Docker/IOT/Client",
      "startTrip": [
        2023,
        12,
        13,
        9,
        0
      ],
      "endTrip": [
        2023,
        12,
        15,
        16,
        56
      ],
      "meetings": [
        {
          "id": 6,
          "title": "One Conference",
          "description": "Key Note on One Conference"
        }
      ]
    }
  ]
  
    const mockAddToWishlist = jest.fn();  

test("is wishlist empty", () => {
    render(<App />);
    const emptyWishlist = screen.getAllByText("Wishlist is empty");
    expect(emptyWishlist.length).toBe(1);
  }
  );
  test("add to wishlist button is clicked", () => {
    render(<TripList addToWishlist={mockAddToWishlist} />);
    const wishlistButton = screen.getAllByText("Add to Wishlist")[0];
    fireEvent.click(wishlistButton);
    expect(mockAddToWishlist).toHaveBeenCalled();
  }
  );
  test("add first trip to wishlist", () => {
    render(<App />);
    const wishlistButton = screen.getAllByText("Add to Wishlist")[0];
    fireEvent.click(wishlistButton);
    const wishlistedTrips = screen.getAllByText("delete Item");
    expect(wishlistedTrips.length).toBe(1);
  }
  );
  
  // this test doesnt work as intended due to bad frontend work (if the frontend was correct this test would work)
  test("delete Item from wishlist (test doesnt work due to bad frontend)", () => {
    render(<App />);
    const wishlistButton = screen.getAllByText("Add to Wishlist")[0]
    fireEvent.click(wishlistButton);
    const wishlistedTrips = screen.getAllByText("delete Item");
    expect(wishlistedTrips.length).toBe(1);
    const deleteButton = wishlistedTrips[0] // the delete button is not working as intended due to bad code
    fireEvent.click(deleteButton);
    expect(wishlistedTrips.length).toBe(1);
  }
  );
  
  test("delete all Items from wishlist", () => {
    render(<App />);
    const wishlistButtons = document.getElementsByClassName("btn btn-outline");
    Array.from(wishlistButtons).forEach((button) => {
      fireEvent.click(button);
    });
  
    const wishlistedTrips = document.getElementsByClassName("wishlistItem");
    expect(wishlistedTrips.length).toBe(3);
    const deleteButtons = document.getElementsByClassName("btn btn-outline-danger");
    const deleteAllButton = deleteButtons[deleteButtons.length - 1]; // Select the last delete button (here we dont have the problem of bad code due to the fact that the delete all works)
    fireEvent.click(deleteAllButton);
    expect(wishlistedTrips.length).toBe(0);
  }
  );
  
  