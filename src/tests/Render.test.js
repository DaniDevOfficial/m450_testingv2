import App from "../App";
import React from "react";
import { render, fireEvent, waitFor, screen } from "@testing-library/react";
import TripList from "../components/TripList";
import Header from "../Header";


test("render App", () => {
    render(<App />);
    const app = screen.getAllByText("Welcome to biztrips Happy new Year-react - 2024");
    expect(app.length).toBe(1);
  }
  );
  test("render Header", () => {
    render(<App />);
    const header = document.getElementsByClassName("header");
    expect(header.length).toBe(1);
  });
  
  test("render wishlist component", () => {
    render(<App />);
    const wishlist = screen.getAllByText("Wishlist");
    expect(wishlist.length).toBe(1);
  }
  );
  test("renders TripList component", () => {
    render(<App />);
    expect(screen.getAllByText("Filter by Month:").length).toBe(1);
  });
  
  test("render Footer", () => {
    render(<App />);
    const footer = document.getElementsByClassName("footer");
    expect(footer.length).toBe(1);
  } 
  );

  